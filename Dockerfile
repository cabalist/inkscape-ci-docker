FROM ubuntu:18.04

# Make sure we don't stall on the tzdata package
ENV DEBIAN_FRONTEND noninteractive
ENV TZ=UTC
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone


RUN apt-get update -qq && \
  apt-get install -qq \
  --no-install-recommends \
# Build Tools
  build-essential \
  ccache \
  clang \
  clang-format \
  clang-tidy \
  clang-tools \
  cmake \
  curl \
  doxygen \
  git \
  intltool \
  libbz2-dev \
  libffi-dev \
  libncurses5-dev \
  libncursesw5-dev \
  libreadline-dev \
  libsqlite3-dev \
  libssl-dev \
  libtool \
  llvm \
  make \
  pkg-config \
  python-dev \
  python-yaml \
  python-setuptools \
  python-wheel \
  python3-setuptools \
  python3-sphinx \
  software-properties-common \
  tk-dev \
  wget \
  xz-utils \
  zlib1g-dev \
# Libraries
  libart-2.0-dev \
  libaspell-dev \
  libblas3 \
  libboost-dev \
  libcdr-dev \
  libgc-dev \
  libgdl-3-dev \
  libglib2.0-dev \
  libgnomevfs2-dev \
  libgsl-dev \
  libgtk-3-dev \
  libgtkmm-3.0-dev \
  libgtkspell-dev \
  libgtkspell3-3-dev \
  libhunspell-dev \
  libjemalloc-dev \
  liblapack3 \
  liblcms2-dev \
  libmagick++-dev \
  libpango1.0-dev \
  libpng-dev \
  libpoppler-glib-dev \
  libpoppler-private-dev \
  libpopt-dev \
  libpotrace-dev \
  librevenge-dev \
  libsigc++-2.0-dev \
  libsoup2.4-dev \
  libvisio-dev \
  libwpg-dev \
  libxml-parser-perl \
  libxml2-dev \
  libxslt1-dev \
  libyaml-dev \
  python-lxml \
  python-pip \
  zlib1g-dev \
# Test Tools
  fonts-dejavu \
  google-mock \
  imagemagick \
  libgtest-dev \
  python3-coverage \
  javascript-common \
  libjs-jquery-isonscreen \
  libjs-jquery-metadata \
  libjs-jquery-tablesorter \
  libjs-jquery-throttle-debounce \
  && apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# Install pyenv for testing extensions
# Inkscape uses the system version of Python (for now) but this makes sure we
#  have a working python environment for all supported versions.

ENV PYENV_ROOT="/.pyenv"
ENV PATH="/.pyenv/bin:/.pyenv/shims:$PATH"
RUN curl -L https://github.com/pyenv/pyenv-installer/raw/master/bin/pyenv-installer | bash
RUN pyenv update
RUN echo eval "$(pyenv init -)" >> /root/pyenv-init
RUN . /root/pyenv-init

RUN pyenv install 2.7.14
RUN pyenv install 3.5.6
RUN pyenv install 3.6.8
RUN pyenv install 3.7.2

# Remove unnecessary pyenv files to keep dockerfile small
RUN find $PYENV_ROOT/versions -type d '(' -name '__pycache__' -o -name 'test' -o -name 'tests' ')' -prune -exec rm -r {} + && \
    find $PYENV_ROOT/versions -type f '(' -name '*.py[co]' -o -name '*.exe' ')' -prune -exec rm {} +

# Tox is used to run python tests and coverage
RUN pip install tox
